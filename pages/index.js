import fetch from 'isomorphic-unfetch';
import _ from 'lodash';

import SongItem from '../components/SongItem';

function Index(props) {
  return (
    <React.Fragment>
      <div>
        <table style={{ margin: 'auto' }}>
          <tbody>
            {_.map(props.data, (song) => SongItem(song))}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
};

Index.getInitialProps = async function () {
  const res = await fetch('http://localhost:3000/api/songs');
  const songs = await res.json();
  return songs;
};

export default Index;
