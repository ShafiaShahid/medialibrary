import fetch from 'isomorphic-unfetch';

function Song(props) {
  const song = props.data;

  return (
    <React.Fragment>
      <table style={{ margin: 'auto' }}>
        <tbody>
          <tr>
            <td><img src={song.album.image} alt="album art" /></td>
            <td style={{ verticalAlign: 'top' }}>
              <div style={{ padding: '1%', width: '100%' }}>
                <span style={{ fontWeight: 'bold', fontSize: 48 }}>{song.title}</span>, <span>{song.album.artist.name}</span>
                <p>{song.album.name} <i>({song.album.year})</i></p>
                <audio style={{ width: '100%', minWidth: 300 }} controls >
                  <source src={song.path} type="audio/mpeg" />
                  Your browser does not support the audio element.
                </audio>
                <div style={{ paddingTop: 10 }} dangerouslySetInnerHTML={{ __html: song.description }}></div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}

Song.getInitialProps = async function (context) {
  const { id = 1 } = context.query;
  const res = await fetch(`http://localhost:3000/api/songs/${id}`);
  const song = await res.json();
  return song;
}

export default Song;
