import Link from 'next/link';

const SongItem = (song) => (
  <tr key={`song_${song.id}`}>
    <td>
      <Link href={`/songs/${song.id}`}>
        <a href={`/songs/${song.id}`}>
          <img src={song.album.image} alt="thumbnail" style={{ width: 100, display: 'inline', padding: 4, backgroundColor: '#fff', border: '1px solid #ddd', borderRadius: 4, transition: 'all .2s ease-in-out', maxWidth: '100%' }} />
        </a>
      </Link>
    </td>
    <td style={{ verticalAlign: 'top' }}>
      <div style={{ padding: '1%', width: '100%' }}>
        <span style={{ fontSize: 24 }}>{song.title}</span>
        <br />
        <span style={{ color: '#808080', fontSize: 20 }}>{song.album.artist.name}</span>
        <br />
        <span>{song.album.name} <i>({song.album.year})</i></span>
        <p>{song.genre}</p>
      </div>
    </td>
    <td>
      <div>
        {song.length}
      </div>
    </td>
  </tr>
);

export default SongItem;
