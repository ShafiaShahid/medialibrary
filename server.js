const express = require('express');
const next = require('next');

const { Album, Song } = require('./database');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare()
  .then(() => {
    const server = express();

    server.get('/api/songs/:id?', (req, res) => {
      const { id } = req.params;

      if (id) {
        Song
          .find({
            where: {
              id,
            },
            include: [
              {
                model: Album,
                include: ['artist'],
              },
            ],
          })
          .then((song) => {
            if (song) {
              res.send({
                data: song,
              });
            } else {
              res.status(404).send();
            }
          })
          .catch(err => {
            console.error(err);
            res.status(500).send();
          });
      } else {
        Song
          .findAll({
            include: [
              {
                model: Album,
                include: ['artist'],
              },
            ],
            order: [
              ['id', 'ASC'],
            ],
          })
          .then((data) => {
            res.send({
              data,
            });
          })
          .catch(err => {
            console.error(err);
            res.status(500).send();
          });
      }
    });

    server.get('/songs/:id', (req, res) => {
      const actualPage = '/song'
      const queryParams = { id: req.params.id }
      app.render(req, res, actualPage, queryParams)
    });

    server.get('*', (req, res) => {
      return handle(req, res)
    });

    server.listen(3000, (err) => {
      if (err) throw err;
      console.log('> Ready on http://localhost:3000');
    });
  })
  .catch((ex) => {
    console.error(ex.stack)
    process.exit(1)
  });
