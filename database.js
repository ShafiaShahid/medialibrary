const Sequelize = require('sequelize');
const sequelize = new Sequelize('media_library', 'root', 'root@123', {
  host: 'localhost',
  port: 3306,
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

const albumPath = '/static/albums';
const trackPath = '/static/tracks';

// models
const Artist = sequelize.define('artist', {
  name: Sequelize.STRING,
});
const Album = sequelize.define('album', {
  name: Sequelize.STRING,
  image: Sequelize.STRING,
  year: Sequelize.INTEGER,
});
const Song = sequelize.define('song', {
  title: Sequelize.STRING,
  description: Sequelize.STRING,
  genre: Sequelize.STRING,
  length: Sequelize.STRING,
  path: Sequelize.STRING,
});

// relationship
Album.belongsTo(Artist);
Song.belongsTo(Album);

// sync data
sequelize
  .sync({
    force: true,
  })
  .then(() => {
    Promise.all([
      Artist.create({
        id: 1,
        name: 'Ariana Grande',
      }),
      Artist.create({
        id: 2,
        name: 'Halsey',
      }),
      Artist.create({
        id: 3,
        name: 'Travis Scott',
      }),
      Artist.create({
        id: 4,
        name: 'Panic! At The Disco',
      }),
      Artist.create({
        id: 5,
        name: 'Post Malone',
      }),
      Artist.create({
        id: 6,
        name: 'J. Cole',
      }),
      Artist.create({
        id: 7,
        name: 'Blueface',
      }),
      Artist.create({
        id: 8,
        name: 'YNW Melly',
      }),
    ])
      .then(() => {
        Promise.all([
          Album.create({
            id: 1,
            name: 'Thank U, Next',
            image: `${albumPath}/thank_u_next.png`,
            year: 2019,
            artistId: 1,
          }),
          Album.create({
            id: 2,
            name: 'Hopeless Fountain Kingdom',
            image: `${albumPath}/hopeless_fountain_kingdom.png`,
            year: 2017,
            artistId: 2,
          }),
          Album.create({
            id: 3,
            name: 'Astroworld',
            image: `${albumPath}/astroworld.jpg`,
            year: 2018,
            artistId: 3,
          }),
          Album.create({
            id: 4,
            name: 'Pray for the Wicked',
            image: `${albumPath}/pray_for_the_wicked.jpg`,
            year: 2018,
            artistId: 4,
          }),
          Album.create({
            id: 5,
            name: 'Single',
            image: `${albumPath}/wow.png`,
            year: 2018,
            artistId: 5,
          }),
          Album.create({
            id: 6,
            name: 'Single',
            image: `${albumPath}/middle_child.jpg`,
            year: 2019,
            artistId: 6,
          }),
          Album.create({
            id: 7,
            name: 'Single',
            image: `${albumPath}/blueface.png`,
            year: 2019,
            artistId: 7,
          }),
          Album.create({
            id: 8,
            name: 'I AM YOU',
            image: `${albumPath}/murder_on_my_mind.jpg`,
            year: 2017,
            artistId: 8,
          }),
        ])
          .then(() => {
            Song.bulkCreate([{
              id: 1,
              title: '7 Rings',
              description: 'Lorem Ipsum',
              path: `${trackPath}/7_rings.mp3`,
              genre: 'Pop',
              length: '3:04',
              albumId: 1,
            }, {
              id: 2,
              title: 'Without Me',
              description: 'Lorem Ipsum',
              path: `${trackPath}/without_me.mp3`,
              genre: 'Pop',
              length: '3:56',
              albumId: 2,
            }, {
              id: 3,
              title: 'Thank U, Next',
              description: 'Lorem Ipsum',
              path: `${trackPath}/thank_u_next.mp3`,
              genre: 'Pop',
              length: '5:30',
              albumId: 1,
            }, {
              id: 4,
              title: 'Sicko Mode',
              description: 'Lorem Ipsum',
              path: `${trackPath}/sicko_mode.mp3`,
              genre: 'Hip-Hop',
              length: '5:23',
              albumId: 3,
            }, {
              id: 5,
              title: 'Break Up with Your Girlfriend, I\'m Bored',
              description: 'Lorem Ipsum',
              path: `${trackPath}/break_up_with_your_girlfriend.mp3`,
              genre: 'Pop',
              length: '3:24',
              albumId: 1,
            }, {
              id: 6,
              title: 'High Hopes',
              description: 'Lorem Ipsum',
              path: `${trackPath}/high_hopes.mp3`,
              genre: 'Indie',
              length: '3:16',
              albumId: 4,
            }, {
              id: 7,
              title: 'Wow',
              description: 'Lorem Ipsum',
              path: `${trackPath}/wow.mp3`,
              genre: 'Hip-Hop',
              length: '2:29',
              albumId: 5,
            }, {
              id: 8,
              title: 'Middle Child',
              description: 'Lorem Ipsum',
              path: `${trackPath}/middle_child.mp3`,
              genre: 'Hip-Hop',
              length: '3:38',
              albumId: 6,
            }, {
              id: 9,
              title: 'Thotiana',
              description: 'Lorem Ipsum',
              path: `${trackPath}/thotiana.mp3`,
              genre: 'Hip-Hop',
              length: '4:00',
              albumId: 7,
            }, {
              id: 10,
              title: 'Murder On My Mind',
              description: 'Lorem Ipsum',
              path: `${trackPath}/murder_on_my_mind.mp3`,
              genre: 'Hip-Hop',
              length: '4:56',
              albumId: 8,
            }]);
          });
      });
  });

module.exports = {
  Album,
  Artist,
  Song,
};
